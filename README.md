hashdiff 1.0.0
==============

Recursively hash files and then GPG sign the hashes. Provides verification and diff functionality.

## Usage

Generating a GPG2 signed list of hashes for a given path:

    ./hashdiff.sh -gen gpg2key@email.address /path1 [...]

Verify a GPG2 signed list of hashes for a given signature file (paths relative or absolute based on how passed in):

    ./hashdiff.sh -verify sigfile.asc
