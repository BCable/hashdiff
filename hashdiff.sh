#!/bin/bash

mode="$1"

if ([[ "$mode" = "-gen" ]] ||
	[[ "$mode" = "-verify" ]]
); then
	shift
else
	mode="-verify"
fi

if [[ "$mode" = "-gen" ]]; then
	key="$1"
else
	sigfile="$1"
fi
shift

# put out usage
if [[ -z "$1" ]]; then
	echo "Usage: $0 [-gen <key>|-verify <sigfile>]"
	exit 1
fi


function header_print(){
	[[ -z "$1" ]] && del="|" || del="$1"
	echo -e "Filename${del}UID${del}GID${del}Perms${del}MD5${del}SHA1${del}SHA256"
}

function hasher(){
	hashtype="$1"
	data="$2"

	if [[ -e "$data" ]]; then
		openssl dgst -r -$hashtype "$data" | cut -d ' ' -f1
	else
		echo "$data" | openssl dgst -r -$hashtype | cut -d ' ' -f1
	fi
}

function hash_one(){
	file="$1"
	filename="$(echo "$file" | cut -d '|' -f1)"

	if [[ -d "$filename" ]]; then
		hashes="dir|dir|dir"

	else
		hash1="$(hasher md5 "$filename")"
		hash2="$(hasher sha1 "$filename")"
		hash3="$(hasher sha256 "$filename")"
		hashes="$hash1|$hash2|$hash3"
	fi

	echo "$file|$hashes"
}

function tree_hash(){
	prefix="$1"
	first="$2"

	if [[ ! -z "$first" ]]; then
		header_print
	fi

	if [[ "${prefix:0:1}" = "/" ]]; then
		short_prefix="$(basename "$prefix")"
		cd "$(dirname "$prefix")"
	else
		short_prefix="$prefix"
	fi

	paths="$(find "$prefix" -printf "%P|%U|%G|%#m\n")"
	echo "$paths" | sort | while read line; do
		if [[ -d "$prefix" ]]; then
			path="$short_prefix/$line"
		else
			path="$short_prefix$line"
		fi

		echo "$(hash_one "$path" "$base_prefix")"
	done
}

function multi_tree_hash(){
	th=""
	first="1"
	for prefix in $@; do
		th="$th$(tree_hash "$prefix" "$first")\n"
		first=""
	done

	echo -e "$th"
}

function gpg_tree_hash(){
	key="$1"
	shift

	multi_tree_hash "$@" | \
		gpg2 --clearsign --armor --default-key "$key" 2> /dev/null

	exit $?
}

function gpg_verify(){
	sigfile="$1"
	retval="$2"

	if [[ ! -e "$sigfile" ]]; then
		echo "$sigfile" | gpg2 --verify 2>&1
	else
		gpgout=$(gpg2 --verify "$sigfile" 2>&1)
	fi

	if [[ ! -z "$retval" ]]; then
		echo $?
	else
		echo $gpgout
	fi
}

function cmp_tree_hash(){
	header_print
	new_tree_data="$(header_print)\n"
	echo "$stripped_data" | tail -n +2 | cut -d '|' -f1 | while read line; do
		begin="$(find "$line" -maxdepth 0 -printf "%p|%U|%G|%#m")"
		hash_one "$begin"
	done
}

function gpg_tree_verify(){
	sigfile="$1"
	prefix="$2"

	# pull data first, this ensures the data GPG verified and the data
	# hash verified isn't subject to a race condition
	data="$(cat "$sigfile")"

	ret=$(gpg_verify "$data" 1)
	if [[ "$ret" != "0" ]]; then
		stripped_data="$(echo "$data" | awk '
			BEGIN { header=1; }
			/BEGIN PGP SIGNATURE/{ exit; }
			/^$/{ header=-1; }
			{
				if(header==-1) header=0;
				else if(header==0) print $0;
			}
		')"

		new_tree_data="$(cmp_tree_hash "$stripped_data")"

		echo "Signature File MD5: $(hasher md5 "$new_tree_data")" 1>&2
		echo "Signature File SHA1: $(hasher sha1 "$new_tree_data")" 1>&2
		echo "Signature File SHA256: $(hasher sha256 "$new_tree_data")" 1>&2
		echo "GPG Verification: Success" 1>&2

		if [[ "$stripped_data" = "$new_tree_data" ]]; then
			echo "Data Hash Verification: Success" 1>&2
			exit 0
		else
			echo "Data Hash Verification: FAILED" 1>&2
			header_print

			diff <(echo "$stripped_data") <(echo "$new_tree_data")
			exit 3
		fi
	else
		echo "GPG Verification: FAILED" 1>&2
		exit 2
	fi
}

echo "Script File MD5: $(hasher md5 "$0")" 1>&2
echo "Script File SHA1: $(hasher sha1 "$0")" 1>&2
echo "Script File SHA256: $(hasher sha256 "$0")" 1>&2

case "$mode" in
	"-gen")
		[[ -z "$key" ]] && echo "No key provided.  Quitting." 1>&2 && exit 1
		gpg_tree_hash "$key" "$@"
		;;

	"-verify")
		if [[ -z "$sigfile" ]]; then
			if [[ "${prefix:${#prefix}-4}" = ".asc" ]]; then
				sigfile="$prefix"
				prefix="${prefix:0:-4}"
			else
				sigfile="$prefix.asc"
			fi
		fi
		gpg_tree_verify "$sigfile" "$prefix"
		;;
esac

echo "ERROR: How'd we get here?" 1>&2
exit 1
